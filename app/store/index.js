export const actions = {
  nuxtServerInit ({ commit }, { req }) {
    if (req.session.authAccount) {
      commit('auth/setUser', req.session.authAccount)
    }
  }
}
