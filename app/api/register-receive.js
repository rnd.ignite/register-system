import axios from 'axios'

export const registerReceiver = (params, option) => axios.post('/api/register/receiver', { params, option })
