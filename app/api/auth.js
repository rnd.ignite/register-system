import axios from 'axios'

export const login = data => axios.post('/auth/login', data)
export const logout = () => axios.get('/auth/logout')
