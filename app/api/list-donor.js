import axios from 'axios'

export const listDonor = params => axios.get('/api/list/donors', { params })
