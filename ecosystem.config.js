const instances = process.env.NODE_INSTANCES ? Number(process.env.NODE_INSTANCES) : 2

module.exports = {
  apps: [
    {
      name: 'register-system',
      script: './server/index.js',
      cwd: '/register-system',
      instances,
      exec_mode: 'cluster',
      node_args: '-r esm'
    }
  ]
}
