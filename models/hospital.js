'use strict';
module.exports = (sequelize, DataTypes) => {
  const hospital = sequelize.define(
    'hospital',
    {
      hospital_id: DataTypes.STRING,
      name: DataTypes.STRING,
      address: DataTypes.STRING
    },
    {}
  );
  hospital.associate = function(models) {
    // associations can be defined here
  };
  return hospital;
};
