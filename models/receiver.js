'use strict'
module.exports = (sequelize, DataTypes) => {
  const receiver = sequelize.define(
    'receiver',
    {
      idnumber: DataTypes.STRING,
      fullname: DataTypes.STRING,
      birthday: DataTypes.DATE,
      email: DataTypes.STRING,
      address: DataTypes.STRING,
      gender: DataTypes.STRING,
      phone: DataTypes.STRING,
      organ: DataTypes.JSON,
      status: DataTypes.STRING,
      blood: DataTypes.STRING
    },
    {}
  )
  receiver.associate = function (models) {
    // associations can be defined here
  }
  return receiver
}
