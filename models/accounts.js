'use strict';
module.exports = (sequelize, DataTypes) => {
  const accounts = sequelize.define(
    'accounts',
    {
      email: DataTypes.STRING,
      name: DataTypes.STRING,
      password: DataTypes.STRING,
      role: DataTypes.STRING,
      hospital_id: DataTypes.STRING
    },
    {}
  );
  accounts.associate = function(models) {
    // associations can be defined here
  };
  return accounts;
};
