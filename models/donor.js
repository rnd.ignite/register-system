'use strict'
module.exports = (sequelize, DataTypes) => {
  const donor = sequelize.define(
    'donor',
    {
      idnumber: DataTypes.STRING,
      fullname: DataTypes.STRING,
      birthday: DataTypes.DATE,
      email: DataTypes.STRING,
      address: DataTypes.STRING,
      gender: DataTypes.STRING,
      phone: DataTypes.STRING,
      organ: DataTypes.JSON,
      secret: DataTypes.BOOLEAN,
      status: DataTypes.STRING,
      blood: DataTypes.STRING
    },
    {}
  )
  donor.associate = function (models) {
    // associations can be defined here
  }
  return donor
}
