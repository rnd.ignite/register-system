# Arguments:
CHANNEL ?= latest
TAG ?= latest
REGISTRY ?= r.cfcr.io

DEPLOYER ?= deploy
SERVER ?= localhost

### Variables:
WEB_IMAGE := $(REGISTRY)/webee/hientang/register-system

# BUILD_ARGS = APP_URL
# DOCKER_BUILD_ARGS = $(foreach arg,$(BUILD_ARGS),--build-arg $(arg)=$($(arg)))

# Build docker images:
build:
	# $(foreach arg,$(BUILD_ARGS),$(if $($(arg)),,$(error Error: $(arg) is not set)))
	docker build . -t $(WEB_IMAGE):$(CHANNEL) --cache-from=$(WEB_IMAGE):$(CHANNEL) $(DOCKER_BUILD_ARGS)

release:
	docker tag $(WEB_IMAGE):$(CHANNEL) $(WEB_IMAGE):$(TAG)
	docker push $(WEB_IMAGE):$(TAG)
	docker push $(WEB_IMAGE):$(CHANNEL)

deploy:
	ssh -t $(DEPLOYER)@$(SERVER) "docker pull $(WEB_IMAGE):$(TAG) && docker pull $(WEB_IMAGE):$(CHANNEL)"
	ssh -t $(DEPLOYER)@$(SERVER) "docker service update ignite_web --image $(WEB_IMAGE):$(TAG) --with-registry-auth --force --detach"
