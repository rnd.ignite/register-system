'use strict';

const md5 = require('crypto-js/md5')
const faker = require('faker')

const ROLE_ADMIN = 'admin'
const ROLE_HOSPITAL = 'hospital'

const generateAccounts = (attrs = { role: ROLE_ADMIN }, total = 30) => {
  for (let id=1; id <= total; id++) {
    const name = faker.name.fullname();
    const email = faker.internet.email();
    const password = md5('secret');

    accounts.push({ name, email, password, ...attrs });
  }

  return accounts
}

module.exports = {
  up: (queryInterface, Sequelize) => {
    const demoAdmins = generateAccounts({ role: ROLE_ADMIN })
    const demoHospitals = generateAccounts({ role: ROLE_HOSPITAL, hospital_id: 1 })

    return Promise.all([
      queryInterface.bulkInsert('accounts', demoAdmins, {}),
      queryInterface.bulkInsert('accounts', demoHospitals, {}),
    ])
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
