'use strict';
const ROLES = require('../../configs/constant').ROLES;
const models = require('../../models');
const { FileSystemWallet, Gateway, X509WalletMixin } = require('fabric-network');
const path = require('path');

exports.connectToNetwork = async function(user) {
  try {
    const ccpPath = path.resolve(__dirname, '../..', 'system-network', `connection-hospital.json`);
    console.log(ccpPath);
    let walletPath = path.join(process.cwd(), `cli/wallet-hospital`);
    console.log(walletPath);

    const wallet = new FileSystemWallet(walletPath);
    const userExists = await wallet.exists(user.email);

    let networkObj;

    if (!userExists) {
      let response = {};
      response.error =
        'An identity for the user ' +
        username +
        ' does not exist in the wallet. Register ' +
        username +
        ' first';
      return response;
    } else {
      const gateway = new Gateway();

      await gateway.connect(ccpPath, {
        wallet: wallet,
        identity: user.email,
        discovery: { enabled: true, asLocalhost: true }
      });

      const network = await gateway.getNetwork('organchannel');
      const contract = await network.getContract('organ');

      networkObj = {
        contract: contract,
        network: network,
        gateway: gateway,
        user: user
      };
    }

    return networkObj;
  } catch (error) {
    console.error(`Failed to evaluate transaction: ${error}`);
    process.exit(1);
  }
};

exports.query = async function(networkObj, func, args) {
  let response = {
    success: false,
    msg: ''
  };
  try {
    if (Array.isArray(args)) {
      response.msg = await networkObj.contract.evaluateTransaction(func, ...args);

      await networkObj.gateway.disconnect();
      response.success = true;
      return response;
    } else if (args) {
      response.msg = await networkObj.contract.evaluateTransaction(func, args);

      await networkObj.gateway.disconnect();
      response.success = true;
      return response;
    } else {
      response.msg = await networkObj.contract.evaluateTransaction(func);

      await networkObj.gateway.disconnect();
      response.success = true;
      return response;
    }
  } catch (error) {
    response.success = false;
    response.msg = error;
    return response;
  }
};

exports.createHospital = async function(networkObj, createdHospital) {
  if (
    !createdHospital.hospital_id ||
    !createdHospital.name ||
    !createdHospital.address ||
    !createdHospital.email
  ) {
    let response = {};
    response.error = 'Error! You need to fill all fields before you can register!';
    return response;
  }

  var orgMSP = 'hospital';
  var nameMSP = 'Hospital';

  try {
    const walletPath = path.join(process.cwd(), `./wallet-hospital`);
    const wallet = new FileSystemWallet(walletPath);

    const hospitalExists = await wallet.exists(createdHospital.email);
    if (hospitalExists) {
      console.log(`An identity for the user ${createdHospital.email} already exists in the wallet`);
      return;
    }

    // Get the CA client object from the gateway for interacting with the CA.
    const ca = await networkObj.gateway.getClient().getCertificateAuthority();
    const adminIdentity = await networkObj.gateway.getCurrentIdentity();

    await networkObj.contract.submitTransaction(
      'CreateHospital',
      createdHospital.hospital_id,
      createdHospital.name,
      createdHospital.address
    );

    await models.hospital.create({
      hospital_id: createdHospital.hospital_id,
      name: createdHospital.name,
      address: createdHospital.address
    });

    await models.accounts.create({
      email: createdHospital.email,
      name: createdHospital.name,
      password: 'secret',
      role: ROLES.HOSPITAL,
      hospital_id: createdHospital.hospital_id
    });

    const secret = await ca.register(
      {
        affiliation: '',
        enrollmentID: hospital.hospital_id,
        role: 'client',
        attrs: [{ name: 'hospitlid', value: createdHospital.hospital_id, ecert: true }]
      },
      adminIdentity
    );

    const enrollment = await ca.enroll({
      enrollmentID: createdHospital.hospital_id,
      enrollmentSecret: secret
    });

    const userIdentity = X509WalletMixin.createIdentity(
      `HospitalMSP`,
      enrollment.certificate,
      enrollment.key.toBytes()
    );

    await wallet.import(createdHospital.email, userIdentity);

    let response = {
      success: true,
      msg: 'Register success!'
    };

    await networkObj.gateway.disconnect();
    return response;
  } catch (error) {
    console.error(`Failed to register!`);
    let response = {
      success: false,
      msg: error
    };
    return response;
  }
};

exports.createDonor = async function(networkObj, createdDonor) {
  if (
    !createdDonor.idnumber ||
    !createdDonor.fullname ||
    !createdDonor.birthday ||
    !createdDonor.email ||
    !createdDonor.address ||
    !createdDonor.gender ||
    !createdDonor.phone ||
    !createdDonor.organ ||
    !createdDonor.secret ||
    createdDonor.status ||
    !createdDonor.blood
  ) {
    let response = {};
    response.error = 'Error! You need to fill all fields before you can register!';
    return response;
  }

  try {
    await Donor.create({
      idnumber: createdDonor.idnumber,
      fullname: createdDonor.fullname,
      birthday: createdDonor.birthday,
      address: createdDonor.address,
      gender: createdDonor.gender,
      phone: createdDonor.phone,
      organ: createdDonor.organ,
      secret: createdDonor.secret,
      status: createdDonor.status,
      blood: createdDonor.blood
    });

    let response = {
      success: true,
      msg: 'Register success!'
    };

    await gateway.disconnect();
    return response;
  } catch (error) {
    console.error(`Failed to register!`);
    let response = {
      success: false,
      msg: error
    };
    return response;
  }
};

exports.createDonation = async function(networkObj, createdDonation) {
  if (
    !createdDonation.idNumber ||
    !createdDonation.organ ||
    !createdDonor.date ||
    !createdDonor.blood ||
    !createdDonor.status
  ) {
    let response = {};
    response.error = 'Error! You need to fill all fields before you can register!';
    return response;
  }
  try {
    await networkObj.contract.submitTransaction(
      'CreateDonation',
      createdDonation.IdNumber,
      createdDonation.Organ,
      createdDonation.Date,
      createdDonation.Blood,
      createdDonation.Status
    );

    let response = {
      success: true,
      msg: 'Register success!'
    };

    await gateway.disconnect();
    return response;
  } catch (error) {
    console.error(`Failed to register!`);
    let response = {
      success: false,
      msg: error
    };
    return response;
  }
};

exports.createReceiver = async function(networkObj, createdReceiver) {
  if (
    !createdReceiver.idnumber ||
    !createdReceiver.fullname ||
    !createdReceiver.birthday ||
    !createdReceiver.email ||
    !createdReceiver.address ||
    !createdReceiver.gender ||
    !createdReceiver.phone ||
    !createdReceiver.organ ||
    !createdReceiver.secret ||
    createdReceiver.status ||
    !createdReceiver.blood
  ) {
    let response = {};
    response.error = 'Error! You need to fill all fields before you can register!';
    return response;
  }

  try {
    await Receiver.create({
      idnumber: createdReceiver.idnumber,
      fullname: createdReceiver.fullname,
      birthday: createdReceiver.birthday,
      address: createdReceiver.address,
      gender: createdReceiver.gender,
      phone: createdReceiver.phone,
      organ: createdReceiver.organ,
      secret: createdReceiver.secret,
      status: createdReceiver.status,
      blood: createdReceiver.blood
    });

    let response = {
      success: true,
      msg: 'Register success!'
    };

    await gateway.disconnect();
    return response;
  } catch (error) {
    console.error(`Failed to register!`);
    let response = {
      success: false,
      msg: error
    };
    return response;
  }
};

exports.createQueue = async function(networkObj, createQueue) {
  if (
    !createQueue.idNumber ||
    !createQueue.organ ||
    !createQueue.date ||
    !createQueue.blood ||
    !createQueue.status
  ) {
    let response = {};
    response.error = 'Error! You need to fill all fields before you can register!';
    return response;
  }
  try {
    await networkObj.contract.submitTransaction(
      'CreateQueue',
      createQueue.IdNumber,
      createQueue.Organ,
      createQueue.Date,
      createQueue.Blood,
      createQueue.Status
    );

    let response = {
      success: true,
      msg: 'Register success!'
    };

    await gateway.disconnect();
    return response;
  } catch (error) {
    console.error(`Failed to register!`);
    let response = {
      success: false,
      msg: error
    };
    return response;
  }
};

exports.transplantation = async function(networkObj, transplantation) {
  if (
    !transplantation.idNumber ||
    !transplantation.organ ||
    !transplantation.status ||
    !transplantation.transplantationDate ||
    !transplantation.hospital_id ||
    transplantation.donorIdNumber
  ) {
    let response = {};
    response.error = 'Error! You need to fill all fields before you can register!';
    return response;
  }
  try {
    await networkObj.contract.submitTransaction(
      'Transplantation',
      transplantation.idNumber,
      transplantation.organ,
      transplantation.status,
      transplantation.transplantationDate,
      transplantation.hospital_id,
      transplantation.donorIdNumber
    );

    let response = {
      success: true,
      msg: 'Register success!'
    };

    await gateway.disconnect();
    return response;
  } catch (error) {
    console.error(`Failed to register!`);
    let response = {
      success: false,
      msg: error
    };
    return response;
  }
};

exports.surgery = async function(networkObj, surgery) {
  if (
    !surgery.idNumber ||
    !surgery.organ ||
    !surgery.status ||
    !surgery.surgeryDate ||
    !surgery.hospital_id
  ) {
    let response = {};
    response.error = 'Error! You need to fill all fields before you can register!';
    return response;
  }
  try {
    await networkObj.contract.submitTransaction(
      'Surgery',
      surgery.idNumber,
      surgery.organ,
      surgery.status,
      surgery.hospital_id,
      surgery.surgeryDate
    );

    let response = {
      success: true,
      msg: 'Register success!'
    };

    await gateway.disconnect();
    return response;
  } catch (error) {
    console.error(`Failed to register!`);
    let response = {
      success: false,
      msg: error
    };
    return response;
  }
};

function changeCaseFirstLetter(params) {
  if (typeof params === 'string') {
    return params.charAt(0).toUpperCase() + params.slice(1);
  }
  return null;
}
