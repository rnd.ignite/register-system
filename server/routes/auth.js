import express from 'express'
import md5 from 'crypto-js/md5'
// import passport from 'passport'
// import { Strategy as LocalStrategy } from 'passport-local'
import models from '../../models'

const router = express.Router()

// passport.use(new LocalStrategy((username, password, done) => {
//   models.findOne({ where: { username, password: md5(password) } })
//     .then(account => done(null, account))
//     .catch(() => done(null, false))
// }))

// router.post('/login', passport.authenticate('local', {
//   successRedirect: '/dashboard',
//   failureRedirect: '/login'
// }))

router.post('/login', async (req, res) => {
  try {
    const { username, password } = req.body

    const account = await models.accounts.findOne({
      attributes: ['id', 'email', 'name', 'role'],
      where: { email: username, password: md5(password).toString() }
    })

    if (account) {
      req.session.authAccount = account
      return res.json(account)
    } else {
      return res.status(401).json({ message: 'Your account does not exists!' })
    }
  } catch (e) {
    return res.status(401).json({ message: 'Your account does not exists!' })
  }
})

router.get('/logout', (req, res) => {
  if (req.session.authAccount) {
    req.session.authAccount = null
  }

  res.sendStatus(200)
})

export default router
