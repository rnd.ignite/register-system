import express from 'express'
import models from '../../../models'

const router = express.Router()

router.post('/register/donor', async (req, res) => {
  const {
    idnumber,
    fullname,
    birthday,
    email,
    address,
    gender,
    phone,
    organ,
    status,
    blood
  } = req.body

  const response = await models.donor.create({
    idnumber,
    fullname,
    birthday,
    email,
    address,
    gender,
    phone,
    organ,
    status,
    blood
  })

  if (response) {
    res.json(response)
  } else {
    res.send('Create error')
  }
})

router.post('/register/receiver', async (req, res) => {
  const {
    idnumber,
    fullname,
    birthday,
    email,
    address,
    gender,
    phone,
    organ,
    status,
    blood
  } = req.body

  const response = await models.receiver.create({
    idnumber,
    fullname,
    birthday,
    email,
    address,
    gender,
    phone,
    organ,
    status,
    blood
  })

  if (response) {
    res.json(response)
  } else {
    res.send('Create error')
  }
})

export default router
