import express from 'express'
import models from '../../../models'

const router = express.Router()

router.get('/list/donors', async (req, res) => {
  if (req.session.authAccount) {
    const page = req.query.page
    const limit = req.query.limit

    const response = await models.donor.findAll({
      attributes: ['fullname', 'idnumber', 'phone', 'email', 'gender', 'status', 'blood', 'birthday', 'organ', 'address'],
      offset: page,
      limit
    })

    res.json(response)
  } else {
    res.status(401).send({ msg: 'You are not login' })
  }
})

router.get('/list/receivers', async (req, res) => {
  if (req.session.authAccount) {
    const page = req.query.page
    const limit = req.query.limit

    const response = await models.receiver.findAll({
      attributes: ['fullname', 'gender', 'status', 'blood'],
      offset: page,
      limit
    })

    res.json(response)
  } else {
    res.status(401).send({ msg: 'You are not login' })
  }
})

export default router
