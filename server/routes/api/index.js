import express from 'express'

import donorRegistrations from './donor-registrations'
import list from './list'
import hospital from './hospital'

const router = express.Router()

router.use(donorRegistrations)
router.use(list)
router.use(hospital)

export default router
