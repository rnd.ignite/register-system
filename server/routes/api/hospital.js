import express from 'express'
import network from '../../fabric/network'

const router = express.Router()

router.post('/hospital/create', async (req, res) => {
  if (req.session.authAccount && req.session.authAccount.role === 'ADMIN') {
    const { name, hospital_id, address, email } = req.body; // eslint-disable-line

    const user = {
      name,
      hospital_id,
      address,
      email
    }

    const networkObj = await network.connectToNetwork(req.session.authAccount)

    if (!networkObj) {
      res.status(500).json({
        success: false,
        msg: 'connect to blockchain err'
      })

      const response = await network.createHospital(networkObj, user)

      if (response.success) {
        res.json({
          success: true,
          msg: 'create success'
        })
      }
    }
  } else {
    res.status(401).send({ msg: 'You are not login' })
  }
})

export default router
