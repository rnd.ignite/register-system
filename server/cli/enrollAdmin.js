'use strict';

const FabricCAServices = require('fabric-ca-client');
const { FileSystemWallet, X509WalletMixin } = require('fabric-network');
const fs = require('fs');
const path = require('path');
const ROLES = require('../../configs/constant').ROLES;
const models = require('../../models');

/**
 * Create admin for Org
 * @param  {String} orgMSP Org Name (default: student)
 * @param  {String} username Admin Username (default: admin)
 */

async function main() {
  try {
    const ccpPath = path.resolve(
      __dirname,
      '../../..',
      'system-network',
      `connection-hospital.json`
    );

    console.log(ccpPath);
    const ccpJSON = fs.readFileSync(ccpPath, 'utf8');
    const ccp = JSON.parse(ccpJSON);

    // Create a new CA client for interacting with the CA.
    const caInfo = ccp.certificateAuthorities[`ca.hospital.organ.com`];
    const caTLSCACertsPath = path.resolve(
      __dirname,
      '../../..',
      'system-network',
      caInfo.tlsCACerts.path
    );
    const caTLSCACerts = fs.readFileSync(caTLSCACertsPath);
    const ca = new FabricCAServices(
      caInfo.url,
      { trustedRoots: caTLSCACerts, verify: false },
      caInfo.caName
    );

    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), `wallet-hospital`);
    const wallet = new FileSystemWallet(walletPath);
    console.log(walletPath);
    // Check to see if we've already enrolled the admin user.
    const adminExists = await wallet.exists(process.env.ADMIN_STUDENT_USERNAME);
    if (adminExists) {
      console.log(
        `An identity for the admin user ${process.env.ADMIN_STUDENT_USERNAME} already exists in the wallet`
      );
      return;
    }

    models.accounts.create({
      password: 'admin',
      email: 'admin@admin.com',
      role: ROLES.ADMIN,
      hospital_id: ''
    });

    // Enroll the admin user, and import the new identity into the wallet.
    const enrollment = await ca.enroll({
      enrollmentID: 'admin',
      enrollmentSecret: 'adminpw'
    });
    const identity = await X509WalletMixin.createIdentity(
      `HospitalMSP`,
      enrollment.certificate,
      enrollment.key.toBytes()
    );
    await wallet.import('admin@admin.com', identity);
    console.log(`Successfully enrolled admin user and imported it into the wallet-hospital`);
    process.exit(0);
  } catch (error) {
    console.error(`Failed to enroll admin user "admin": ${error}`);
    process.exit(1);
  }
}

function changeCaseFirstLetter(params) {
  if (typeof params === 'string') {
    return params.charAt(0).toUpperCase() + params.slice(1);
  }
  return null;
}

main();
