import consola from 'consola'
import chokidar from 'chokidar'
import _last from 'lodash/last'
import _debounce from 'lodash/debounce'
import { nuxt, build } from './nuxt'
import createServer from './express'

const { host, port } = nuxt.options.server

function invalidateRequireCache () {
  Object.keys(require.cache).forEach((id) => {
    if (id.startsWith(__dirname)) {
      delete require.cache[id]
    }
  })
}

function setServerMiddleware (nuxtInstance) {
  const express = createServer()

  nuxtInstance.options.serverMiddleware = [express]

  return express
}

setServerMiddleware(nuxt)

build().then(async () => {
  consola.info('Server starting')

  await nuxt.listen()

  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })

  const listener = _last(nuxt.server.listeners)

  chokidar.watch('server/**/*.js', {
    ignored: [
      'server/nuxt.js',
      'server/dev.js',
      'server/index.js'
    ]
  }).on('change', _debounce(async () => {
    try {
      consola.log('')
      consola.info('Server files changed. Restarting...')

      // Shut down
      await listener.close()

      invalidateRequireCache()

      // Dirty hack below
      // Empty `connect`'s middleware stack and force renderer to reload
      setServerMiddleware(nuxt)
      nuxt.renderer.app.stack = []
      await nuxt.server.setupMiddleware()
      await listener.listen()

      consola.success('Updated')
    } catch (error) {
      consola.error(error)
    }
  }, 250))
})
