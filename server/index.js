import consola from 'consola'
import { nuxt } from './nuxt'
import createServer from './express'

const { host, port } = nuxt.options.server
const express = createServer()

nuxt.options.serverMiddleware = [express]
nuxt.listen()

consola.ready({
  message: `Server listening on http://${host}:${port}`,
  badge: true
})
