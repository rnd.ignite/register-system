import express from 'express'
import session from 'express-session'
import passport from 'passport'
import flash from 'connect-flash'
import bodyParser from 'body-parser'
import mysqlSession from 'express-mysql-session'
import mysql from 'mysql'
import { development } from '../config/config'

import api from './routes/api'
import auth from './routes/auth'

const app = express()

const MySQLStore = mysqlSession(session)

const pool = mysql.createPool(development)

pool.getConnection((err, connection) => {
  if (err) {
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      console.error('Database connection was closed.')
    }
    if (err.code === 'ER_CON_COUNT_ERROR') {
      console.error('Database has to many connections')
    }
    if (err.code === 'ECONNREFUSED') {
      console.error('Database connection was refused')
    }
  }

  if (connection) { connection.release() }
  console.log('DB is Connected')
})

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(
  session({
    secret: 'process.env.SESSION_SECRET',
    resave: false,
    saveUninitialized: false,
    store: new MySQLStore(development)
  })
)

app.use(flash())
app.use(passport.initialize())
app.use(passport.session())

app.use('/api', api)
app.use('/auth', auth)

app.use('/dashboard', (req, res, next) => {
  if (!req.session.authAccount) {
    return res.redirect('/login')
  } else {
    next()
  }
})

app.use((req, res, next) => {
  app.locals.message = req.flash('message')
  app.locals.success = req.flash('success')
  app.locals.user = req.user
  next()
})

export default () => app
