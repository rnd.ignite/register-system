import { Nuxt, Builder } from 'nuxt'
import config from '../nuxt.config'

config.dev = process.env.NODE_ENV !== 'production'

// Init Nuxt.js
export const nuxt = new Nuxt(config)

export const build = () => {
  const builder = new Builder(nuxt)
  return builder.build()
  // return Promise.resolve(nuxt);
}
