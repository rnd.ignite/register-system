#---------------------------------------
#   Final build
#---------------------------------------

FROM keymetrics/pm2:10-alpine

WORKDIR /register-system

COPY package.json yarn.lock ./

RUN yarn install --frozen-lockfile

ARG APP_URL=https://hientang.webee.asia

ENV NODE_INSTANCES=2 \
    NODE_ENV=production \
    APP_URL=${APP_URL}

COPY . .

RUN yarn build

EXPOSE 3000

CMD [ "pm2-runtime", "start", "ecosystem.config.js" ]
