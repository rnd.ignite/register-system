module.exports = {
  development: {
    user: process.env.DB_USERNAME || 'register_system',
    username: process.env.DB_USERNAME || 'register_system',
    password: process.env.DB_PASSWORD || 'secret',
    database: process.env.DB_DATABASE || 'register_system',
    host: process.env.DB_HOST || 'mysql',
    dialect: 'mysql'
  },
  test: {
    username: process.env.DB_USERNAME || 'register_system',
    password: process.env.DB_PASSWORD || 'secret',
    database: process.env.DB_DATABASE || 'register_system',
    host: process.env.DB_HOST || '127.0.0.1',
    dialect: 'mysql'
  },
  production: {
    username: process.env.DB_USERNAME || 'register_system',
    password: process.env.DB_PASSWORD || 'secret',
    database: process.env.DB_DATABASE || 'register_system',
    host: process.env.DB_HOST || '127.0.0.1',
    dialect: 'mysql'
  }
}
